import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom'
import MainProduct from './components/MainProduct';

function App() {
  return (
    <div>
       <Routes>
          <Route index element={<MainProduct></MainProduct>}></Route>
        </Routes> 
    </div>
  );
}

export default App;
